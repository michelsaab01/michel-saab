#prints a message on the screen stating whats in between quotations in red
echo "Running the input preparation for the turbulent simulation"
#declaring n = 1
n=1
#for loop for Re number
for Re in 10000 50000 100000 500000 1000000
do
	#declaring variable m
m=1
#for loop for the mesh in the x direction
for xMesh in 512 1024
do
	#declaring variable o
o=1
#for loop for the mesh in the y direction 
for yMesh in 128 256
do
	#creates a dirrectory called simulation with the values of n m and o 
mkdir Michel${n}_${m}_${o}
#changes directory to originalF
cd originalF
#replaces rrrrrrr wth the value of Re and xxxx with the Value of xMesh and yyy with the value of yMesh
sed -e "s/rrrrrrr/${Re}/" -e "s/xxxx/${xMesh}/" -e "s/yyy/${yMesh}/" inputOrig > input.dat
echo "we are setting the value of Re = $Re for xmesh = $xMesh and ymesh= $yMesh"
#coppiies input.data to simulation with the corresponding values of n m and o depedning on the loop number reached
cp input.dat ../Michel${n}_${m}_${o}
#changes directory back to parent folder
cd ../
#incremneting o
o=$(( $o + 1 ))
#closes the y mesh loop
done
#incrementing m
m=$(( $m + 1 ))
#closing the xmesh loop
done
#incrementing n
n=$(( $n + 1 ))
#closing the Re loop
done
#prints a message on the screen 
echo "input file preparation is done"
